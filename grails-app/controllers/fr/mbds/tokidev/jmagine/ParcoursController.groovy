package fr.mbds.tokidev.jmagine

import grails.converters.XML
import grails.plugin.springsecurity.SpringSecurityService
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.commons.CommonsMultipartFile

class ParcoursController {
    ParcoursService parcoursService
    SpringSecurityService springSecurityService
    PoiService poiService
    ImageService imageService
    RightsService rightsService

    // Protégé par Config.groovy: ROLE_ADMIN/ROLE_OP/ROLE_MOD
    // Vérification nécessaire : Ne lister que les parcours modérés si l'user est ROLE_MOD
    def list() {
        User me = springSecurityService.getCurrentUser()
        if(rightsService.simpleRoleCheck( me, 'ROLE_ADMIN')) {
            def parcours = Parcours.withCriteria {
                order('dateCreated', 'desc')
            }
            render(view: '/parcours/list', model: [me: me, parcours_list: parcours])
        }
        else {
            render(view: '/parcours/list', model: [me: me, parcours_list: me.moderatedParcours])
        }
    }

    // Protégé par Config.groovy: ROLE_ADMIN/ROLE_OP/ROLE_MOD
    // Vérification nécessaire : On ne peut éditer que les parcours dont on est le modérateur, a moins d'être ROLE_ADMIN ou ROLE_OP
    def edit(Long p_id) {
        def parcours = Parcours.get(p_id)
        if (parcours) {
            User me = springSecurityService.getCurrentUser()
            if(rightsService.simpleRoleCheck( me, 'ROLE_ADMIN')){
                render(view: '/parcours/edit', model: [me: me, parcours: parcours])
            }
            else if( parcours in me.moderatedParcours ){
                render(view: '/parcours/edit', model: [me: me, parcours: parcours])
            }
            else {
                response.sendError(403)
            }
        }
        else {
            response.sendError(404)
        }
    }

    // Protégé par Config.groovy: ROLE_ADMIN/ROLE_OP
    def add() {
        User me = springSecurityService.getCurrentUser()
        render(view: '/parcours/add', model: [me: me])
    }

    // Protégé par Config.groovy: ROLE_ADMIN/ROLE_OP
    def do_add() {
        MultipartFile file
        try {
            file = request.getFile("background_pic")
        }
        catch (e) {

        }

        User me = springSecurityService.getCurrentUser()

        if (params.title && params.title != '') {
            Parcours parcours = parcoursService
                    .createParcours(title: params.title, description: params.description, imageType: params.image_type, background_picture: file, author: me)
            redirect(controller: 'parcours', action: 'edit', params: [p_id: parcours.id])
        } else {
            flash.errors = [title: true];
            flash.fields = [
                    description: params.description
            ]
            redirect(controller: 'parcours', action: 'add')
        }
    }

    // Protégé par Config.groovy: ROLE_ADMIN/ROLE_OP/ROLE_MOD
    // Vérification nécessaire : On ne peut éditer que les parcours dont on est le modérateur, a moins d'être ROLE_ADMIN ou ROLE_OP
    def do_info_edit(Long p_id) {
        Parcours parcours = Parcours.get(p_id)
        if (parcours) {
            User me = springSecurityService.getCurrentUser()
            if( rightsService.simpleRoleCheck( me, 'ROLE_ADMIN') || ( parcours in me.moderatedParcours )){
                CommonsMultipartFile file
                try {
                    file = request.getFile("background_pic")
                }
                catch (e) {

                }

                if (params.title && params.title != '') {
                    parcoursService.updateParcours(parcours: parcours, title: params.title, description: params.description, background_picture: file, imageType: params.image_type, imageId: params.image_id)
                    redirect(controller: 'parcours', action: 'edit', params: [p_id: parcours.id])
                } else {
                    flash.errors = [title: true];
                    flash.fields = [
                            description: params.description
                    ]
                    redirect(controller: 'parcours', action: 'edit', params: [p_id: parcours.id])
                }
            }
            else {
                response.sendError(403)
            }
        }
        else {
            response.sendError(404)
        }
    }

    // Protégé par Config.groovy: ROLE_ADMIN/ROLE_OP/ROLE_MOD
    // Vérification nécessaire : On ne peut supprimer un parcours que si on est ROLE_ADMIN ou ROLE_OP
    def delete( Long p_id ) {
        Parcours parcours = Parcours.get( p_id )
        User me = springSecurityService.getCurrentUser()

        if( parcours ) {
            if( rightsService.simpleRoleCheck( me, 'ROLE_ADMIN') ) {
                parcours.pois.collect().each { POI poi ->
                    parcours.removeFromPois(poi)
                    parcours.save()
                }
                parcours.components.collect().each { ContentComponent component ->
                    parcours.removeFromComponents(component)
                    parcours.save()
                }
                parcours.backgroundPic = null;
                parcours.save()

                parcours.fileList.collect().each { FileContainer fileContainer ->
                    imageService.deleteImage(fileContainer)
                    parcours.removeFromFileList(fileContainer)
                }
                parcours.save()
                parcours.delete(flush: true)
                redirect(controller: "parcours", action: "list")
            }
            else {
                response.sendError(403)
            }
        }
        else {
            response.sendError(404)
        }
    }

    // Protégé par Config.groovy: ROLE_ADMIN/ROLE_OP/ROLE_MOD
    // Vérification nécessaire : On ne peut activer/desactiver un parcours que si on est ROLE_ADMIN ou ROLE_OP
    def enable( Long p_id ) {
        Parcours parcours = Parcours.get(p_id)
        User me = springSecurityService.getCurrentUser()

        if (parcours) {
            if( rightsService.simpleRoleCheck( me, 'ROLE_ADMIN') ) {
                parcoursService.toogleParcoursStatus( parcours, true )
                redirect( controller: 'parcours', action:'list' )
            }
            else {
                response.sendError(403)
            }
        } else {
            response.sendError(404)
        }
    }

    // Protégé par Config.groovy: ROLE_ADMIN/ROLE_OP/ROLE_MOD
    // Vérification nécessaire : On ne peut activer/desactiver un parcours que si on est ROLE_ADMIN ou ROLE_OP
    def disable( Long p_id ) {
        Parcours parcours = Parcours.get(p_id)
        User me = springSecurityService.getCurrentUser()

        if (parcours) {
            if( rightsService.simpleRoleCheck( me, 'ROLE_ADMIN') ) {
                parcoursService.toogleParcoursStatus( parcours, false )
                redirect( controller: 'parcours', action:'list' )
            }
            else {
                response.sendError(403)
            }
        } else {
            response.sendError(404)
        }
    }

    // API

    // Ouvert a tous
    def api_get(Long p_id) {
        Parcours parcours = Parcours.get(p_id)

        if( parcours ) {
            if( parcours.isValidated ) {
                withFormat {
                    xml {
                        XML.use('api_basic') {
                            render parcours as XML
                        }
                    }
                }
            }
            else {
                response.sendError(403)
            }
        }
        else {
            response.sendError(404)
        }
    }

    // Ouvert a tous
    def api_get_from_title( Long p_id ) {
        Parcours parcours = Parcours.get( p_id )

        if( parcours ) {
            if( parcours.isValidated ) {
                withFormat {
                    xml {
                        XML.use('api_basic') {
                            render parcours as XML
                        }
                    }
                }
            }
            else {
                response.sendError(403)
            }
        }
        else {
            response.sendError(404)
        }
    }

}
