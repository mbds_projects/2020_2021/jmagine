package fr.mbds.tokidev.jmagine

class JmagineImageTagLib {
    static defaultEncodeAs = [taglib:'raw']

    def jmagineImage = { attrs, body ->
        def uri
        if(!attrs.class) attrs.class = ''

        if( attrs.type=="background")
            uri = asset.image(src:'default_background.png', class:attrs.class )
        else
            uri = asset.image(src:'default_avatar.png', class:attrs.class )

        if(attrs.makelink && attrs.src) {
            if( attrs.absolute )
                out << grailsApplication.config.grails.assetspath.absolute_images+attrs.src
            //               out << grailsApplication.config.grails.assetspath.images+attrs.src

        }
        else if(attrs.src) {
            out << "<img src=\"${( grailsApplication.config.grails.assetspath.relative_images + attrs.src)}\" class=\"${attrs.class}\" />"
            //            out << "<img src=\"${( grailsApplication.config.grails.assetspath.images + attrs.src)}\" class=\"${attrs.class}\" />"

        }
        else {
            out << uri
        }
    }
}
