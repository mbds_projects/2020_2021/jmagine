
<!-- TODO replace message code - error asset-->
<!DOCTYPE html>
<html>
	<head>
		<title><g:message code="jmagine.titles.404"/> - jMagine</title>
		<meta name="layout" content="backend_error">
	</head>
	<body>
		<div class="main_error">
			<div class="logo">
				<asset:image src="logo_small_color.png"/>
			</div>
			<g:message code="jmagine.generic.404"/>
		</div>
	</body>
</html>
