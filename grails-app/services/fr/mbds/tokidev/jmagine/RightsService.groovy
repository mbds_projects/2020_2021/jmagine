package fr.mbds.tokidev.jmagine

import grails.gorm.transactions.Transactional

@Transactional
class RightsService {

    def simpleRoleCheck(User user, String authority) {
        if (user && authority) {
            Role role = Role.findByAuthority(authority)
            if (role) {
                Role role_admin = Role.findByAuthority('ROLE_ADMIN')
                Role role_op = Role.findByAuthority('ROLE_OP')

                if (authority == 'ROLE_OP') {
                    def roles = UserRole.findAllByUser(user)
                    return roles.role.contains(role)
                } else if (authority == 'ROLE_ADMIN') {
                    def roles = UserRole.findAllByUser(user)
                    return roles.role.contains(role_admin) || roles.role.contains(role_op)
                } else if (authority == 'ROLE_MOD') {
                    def roles = UserRole.findAllByUser(user)
                    return roles.role.contains(role_admin) || roles.role.contains(role_op) || roles.role.contains(role)
                }
            }
        }
        return false
    }

    def hasHigherRole(User userA, User userB) {
        Role roleA = userA.getAuthorities()[0]
        Role roleB = userB.getAuthorities()[0]

        if (roleB.authority == 'ROLE_OP') {
            return roleA.level == roleB.level
        } else return roleA.level > roleB.level
    }
}
