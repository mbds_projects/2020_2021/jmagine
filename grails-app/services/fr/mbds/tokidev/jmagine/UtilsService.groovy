package fr.mbds.tokidev.jmagine

import grails.gorm.transactions.Transactional

import javax.mail.internet.AddressException
import javax.mail.internet.InternetAddress

@Transactional
class UtilsService {

    def isValidPassword(String password) {
        return password && (password.size() > 5)
    }

    def isEmailValid(String email) {
        return isValidEmailAddress(email)
    }

    static boolean isValidEmailAddress(String email) {
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate()
        } catch (AddressException ex) {
            return false
        }
        return true
    }
}
