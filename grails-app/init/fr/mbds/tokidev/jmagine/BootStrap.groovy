package fr.mbds.tokidev.jmagine

import grails.converters.XML
import grails.gorm.transactions.Transactional
import org.springframework.mock.web.MockMultipartFile

class BootStrap {

    PoiService poiService
    ParcoursService parcoursService
    UserService userService

    def init = { servletContext ->
        addUsers()
    }
    def destroy = {
    }

    @Transactional
    void addUsers() {
        XML.createNamedConfig('api_basic') { it.registerObjectMarshaller(new GenericMarshaller()) }

        if (!Role.findByAuthority('ROLE_OP')) {
            User userOp, userAdmin, userMod

            def roleOp = new Role(authority: 'ROLE_OP', level: 3).save(flush: true, failOnError: true)
            def roleAdmin = new Role(authority: 'ROLE_ADMIN', level: 2).save(flush: true, failOnError: true)
            def roleModerator = new Role(authority: 'ROLE_MOD', level: 1).save(flush: true, failOnError: true)

            try {
                FileInputStream user_op_fis =
                        new FileInputStream(new File("grails-app\\assets\\images\\user_01.jpg"))
                MockMultipartFile user_op_mmpf = new MockMultipartFile("test.txt", "test.png", "image/png", user_op_fis)

                userOp = userService.createUser(imageType: 'upload',
                        username: "lionel_tokidev",
                        password: "password",
                        mail: "lionel@tokidev.fr",
                        role: roleOp,
                        thumbnail: user_op_mmpf)

                userAdmin = userService
                        .createUser(username: "admin",
                                password: "password",
                                mail: "admin@tokidev.fr",
                                role: roleAdmin,
                                thumbnail: null)

                userMod = userService
                        .createUser(username: "modo",
                                password: "modo",
                                mail: "modo@tokidev.fr",
                                role: roleModerator,
                                thumbnail: null)

                (0..10).each {
                    userService
                            .createUser(username: "modo" + it,
                                    password: "modo",
                                    mail: "modo@tokidev" + it + ".fr",
                                    role: roleModerator,
                                    thumbnail: null)
                }

            }
            catch (e) {
                e.printStackTrace()
                println "Erreur durant le chargement du fichier"
            }
            try {


                FileInputStream inputStream_parcours =
                        new FileInputStream(
                                new File("grails-app\\assets\\images\\musee_arts_asiatiques.jpg"))

                MockMultipartFile bg_parcours =
                        new MockMultipartFile("test.txt",
                                "test.png", "image/png", inputStream_parcours)

                Parcours parcours = parcoursService
                        .createParcours(title: 'Musées Nice',
                                imageType: 'upload', background_picture: bg_parcours,
                                description: 'Test', author: userOp)

                parcoursService.addComponent(title: 'Composant', imageType: 'upload',
                        'background_picture': bg_parcours, content: '<b>Huehuehue</b>', parcours: parcours)

                FileInputStream inputStream1 =
                        new FileInputStream(new File("grails-app\\assets\\images\\musee_arts_asiatiques.jpg"))
                MockMultipartFile bg_musee_arts_asiatiques =
                        new MockMultipartFile("test.txt", "test.png", "image/png", inputStream1)

                FileInputStream inputStream2 =
                        new FileInputStream(new File("grails-app\\assets\\images\\musee_beaux_arts.jpg"))
                MockMultipartFile bg_musee_beaux_arts =
                        new MockMultipartFile("test.txt", "test.png", "image/png", inputStream2)

                FileInputStream inputStream3 = new FileInputStream(new File("grails-app\\assets\\images\\musee_marc_chagall.jpg"))
                MockMultipartFile bg_marc_chagall = new MockMultipartFile("test.txt", "test.png", "image/png", inputStream3)

                FileInputStream inputStream4 = new FileInputStream(new File("grails-app\\assets\\images\\musee_mamac.jpg"))
                MockMultipartFile bg_mamac = new MockMultipartFile("test.txt", "test.png", "image/png", inputStream4)

                FileInputStream inputStream5 = new FileInputStream(new File("grails-app\\assets\\images\\musee_matisse.jpg"))
                MockMultipartFile bg_matisse = new MockMultipartFile("test.txt", "test.png", "image/png", inputStream5)

                poiService.createPOI(imageType: 'upload', parcours: parcours, lat: '43.668065', lng: '7.216075', title: 'Musée des Arts Asiatiques', address: '405 Promenade des Anglais, 06200 Nice', author: userOp, background_picture: bg_musee_arts_asiatiques)
                poiService.createPOI(imageType: 'upload', parcours: parcours, lat: '43.694539', lng: '7.248851', title: 'Musée des Beaux Arts de Nice', address: '33 Avenue des Baumettes, 06000 Nice', author: userOp, background_picture: bg_musee_beaux_arts)
                poiService.createPOI(imageType: 'upload', parcours: parcours, lat: '43.709137', lng: '7.269403', title: 'Musée National Marc Chagall', address: '36 Avenue Docteur Ménard, 06000 Nice', author: userOp, background_picture: bg_marc_chagall)
                poiService.createPOI(imageType: 'upload', parcours: parcours, lat: '43.70146', lng: '7.278485', title: 'Musée d\'Art Moderne et d\'Art Contemporain', address: 'Place Yves Klein, 06364 Nice', author: userOp, background_picture: bg_mamac)
                poiService.createPOI(imageType: 'upload', parcours: parcours, lat: '43.719536', lng: '7.275381', title: 'Musée Matisse', address: '164 Avenue des Arènes de Cimiez, 06000 Nice', author: userOp, background_picture: bg_matisse)
            }
            catch (e) {
                e.printStackTrace()
                println 'erreur creation pois/parcours'
            }
        }
    }
}
