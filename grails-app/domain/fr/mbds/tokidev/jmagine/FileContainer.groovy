package fr.mbds.tokidev.jmagine



/**
 * Serves all purposes for file handling, can be files for the frontend presentation or files obtained from comments
 * Each file can be submitted with a comment to help tracking
 * fr.mbds.tokidev.jmagine.FileType must be defined for each file
 * Files are linked to the "Parcours" to help segmentation between different projects
 * The user should be tracked in order to prevent abusive use of the comment service
 */
class FileContainer {

    FileType    type
    String      filename
    String      comment
    Long        ownerId // Can be intern User / MobileUser depending on the ownerType
    OwnerType   ownerType
    Date        dateCreated


    static belongsTo = [ parcours:Parcours ]

    static constraints =
            {
                type                nullable: false
                filename            blank: false
                comment             blank: true, nullable: true
                ownerId             nullable: false
                ownerType           nullable: false
            }
}
