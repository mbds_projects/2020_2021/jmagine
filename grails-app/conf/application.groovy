// Added by the Spring Security Core plugin:
grails.plugin.springsecurity.userLookup.userDomainClassName = 'fr.mbds.tokidev.jmagine.User'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'fr.mbds.tokidev.jmagine.UserRole'
grails.plugin.springsecurity.authority.className = 'fr.mbds.tokidev.jmagine.Role'
grails.plugin.springsecurity.securityConfigType = "InterceptUrlMap"
grails.plugin.springsecurity.failureHandler.defaultFailureUrl = "/auth?login_error=1"
grails.plugin.springsecurity.auth.loginFormUrl = "/login/auth"
grails.plugin.springsecurity.apf.filterProcessesUrl = "/j_spring_security_check"

grails.plugin.springsecurity.interceptUrlMap = [
        [pattern: '/', access: ['ROLE_ADMIN', 'ROLE_OP', 'ROLE_MOD']],
        [pattern: '/assets/**', access: ['permitAll']],
        [pattern: '/uploads/**', access: ['permitAll']],
        [pattern: '/ck/**', access: ['permitAll']],
        [pattern: '/videos/**', access: ['permitAll']],
        [pattern: '/**/js/**', access: ['permitAll']],
        [pattern: '/**/css/**', access: ['permitAll']],
        [pattern: '/**/images/**', access: ['permitAll']],
        [pattern: '/**/favicon.ico', access: ['permitAll']],
        [pattern: '/parcours/add', access: ['ROLE_ADMIN', 'ROLE_OP']],
        [pattern: '/parcours/do_add', access: ['ROLE_ADMIN', 'ROLE_OP']],
        [pattern: '/parcours/**/moderators/**', access: ['ROLE_ADMIN', 'ROLE_OP']],
        [pattern: '/parcours/**', access: ['ROLE_ADMIN', 'ROLE_OP', 'ROLE_MOD']],
        [pattern: '/my_account', access: ['ROLE_ADMIN', 'ROLE_OP', 'ROLE_MOD']],
        [pattern: '/my_account/**', access: ['ROLE_ADMIN', 'ROLE_OP', 'ROLE_MOD']],
        [pattern: '/users/**', access: ['ROLE_ADMIN', 'ROLE_OP']],
        [pattern: '/fonts/**', access: ['permitAll']],
        [pattern: '/qrcode/**', access: ['ROLE_ADMIN', 'ROLE_OP', 'ROLE_MOD']],
        [pattern: '/dbconsole/**', access: ['permitAll']],
        [pattern: '/api/**', access: ['permitAll']],
        [pattern: '/page_not_found', access: ['permitAll']],
        [pattern: '/page_forbidden', access: ['permitAll']],
        [pattern: '/j_spring_security_check', access: ['permitAll']],
        [pattern: '/authfail', access: ['permitAll']],
        [pattern: '/login/auth', access: ['permitAll']],
        [pattern: '/h2-console/**', access: ['permitAll']]
]
grails.plugin.springsecurity.controllerAnnotations.staticRules = [
        '/'              : ['permitAll'],
        '/index'         : ['permitAll'],
        '/index.gsp'     : ['permitAll'],
        '/assets/**'     : ['permitAll'],
        '/**/js/**'      : ['permitAll'],
        '/**/css/**'     : ['permitAll'],
        '/**/images/**'  : ['permitAll'],
        '/**/favicon.ico': ['permitAll']
]

