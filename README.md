# Jmagine backend

## L'histoire
Le projet Jmagine est la genèse de plusieurs projets portés par le MBDS. Ses premières moutures sont nées avant la généralisation des smartphones, avant 2010 et ont donné lieu à la production de quelques "Proof of Concept" démontrés notamment à Menton en Juin 2010 La démonstration a donné lieu à une petite séquence sur FR3 : https://youtu.be/uaxH-MxA2Hk.

Sur les années qui ont suivies, le projet a été repensé et refait pour donner lieu à des réalisations destinées aux smartphones tels que nous les connaissons. La nouvelle version est née en 2014 et a été inaugurée à l'occasion de la 5ème édition d'Ecritech qui a eu lieu à Nice au Lycée Saint Jean d'Angély Quasiment chaque année depuis sa refonte, nous avons fait des déploiements spécifiques dans différents lieux.

## Technologies employées

Pour rendre les productions et l'utilisation de l'application plus ludique, nous avons intégré de nombreuses technologies pour apporter un côté "fun" au projet. Nous pouvons situer une personne et pousser du contenu sur son téléphone de 5 manières différentes :

- Tag NFC (sans contact)
- QR Code (classique) TPT 2020/2021
- Géolocalisation (GPS / GPRS) pour les chemins en extérieurs ou les endroits que nous ne pouvons pas toucher (pas de cartel / pancarte)
- LIFI (Localisation via des luminaires) que nous utilisons principalement pour le positionnement indoor (souvent dans les musées)
- Reconnaissance d'objet, nous avons expérimenté et déployé un moteur de reconnaissance d'objet qui nous a permis, lors de la démonstration au vieux Nice de reconnaître directement les différents bâtiments en les prenant en photo.

Nous avons aussi tout un panel d'interactions possible avec des éléments extérieurs, lors des différentes expérimentations, nous avons par exemple déclenché des diffusions de pico projecteur sur des murs / déclenché des diffusions d'odeur lors que les utilisateurs arrivaient à certains points clef pour certains parcours.

Le système que nous avons mis en place est quasiment sans limite, nous sommes aujourd'hui capables d'interagir avec n'importe quel équipement à partir du moment où ce dernier est "connecté".

## Version

**Grails**  &rarr; 4.0.1

**SDK**     &rarr; Java v 1.8.0_261


### Authors 

_In progress_
